dev:
	npm run dev

build:
	npm run build

push:
	ssh -i ~/.ssh/digital dev@recibosypagos.co 'rm -rf /home/dev/Projects/umdb/dist'
	scp -i ~/.ssh/digital -r dist/ dev@recibosypagos.co:/home/dev/Projects/umdb
